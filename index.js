const path = require('path');
const fs = require('fs');
const program = require('commander');
const crypto = require('crypto');
const dateFormat = require('dateformat');
const JSZip = require('jszip');

let isMac = false;

const showErr = (err) => {
  console.log(err);
  process.exit();
}

const end = (out, res) => {
  fs.writeFileSync(out, res);
  console.log(res); 
  process.exit(); 
}

const getHash = (fileBuf) => {
  const hash = crypto.createHash('sha512');
  hash.update(fileBuf);
  const hshBuff = Buffer.from(hash.digest());
  return hshBuff.toString('base64');  
}
 
program
  .option('-f, --file <path>', 'file path for generate yml file')
  .option('-c, --channel <channel>', 'update channel')
  .option('-n --now', 'set current datetime for releaseDate field')
  .option('-v --ver <version>', 'program version')
  .option('-F --force', 'update exists yml file')
  .parse(process.argv);

if (!program.file) {
  showErr('No input data. Please use --file <path>');
}

if (!fs.existsSync(program.file)) {
  showErr('File not found');
}

const fileName = path.parse(program.file);

if (fileName.ext === '.dmg') {
  isMac = true;
}

const channel = program.channel || 'latest';
const prefix = isMac ? '-mac' : '';

const out = `${fileName.dir}/${channel}${prefix}.yml`;
if (!program.force && fs.existsSync(out)) {
  showErr(`Output file ${out} is allredy exists. Please use --force`);
}

const fileContent = fs.readFileSync(program.file);
const fileHash = getHash(fileContent);

const fileInfo = fs.lstatSync(program.file);
const v = fileName.name.replace(/.*?\-(.*?)/, '$1');
const dt = dateFormat(program.now ? new Date() : fileInfo.mtime, 'isoUtcDateTime');

let res = '';

if (!isMac) {

res = `version: ${program.ver || v || '1.0.0'}
files:
  - url: ${fileName.base}
    sha512: ${fileHash}
    size: ${fileInfo.size}
path: ${fileName.base}
sha512: ${fileHash}
releaseDate: '${dt}'
`;

end(out, res);

}

const zip = new JSZip();
zip.file(fileName.base, fileContent);
zip.generateAsync({type: 'nodebuffer'}).then((zipContent) => {

const zipName = `${fileName.name}-osx.zip`;
const zipHash = getHash(zipContent);
const zipPath = `${fileName.dir}/${zipName}`;
fs.writeFileSync(zipPath, zipContent);
const zipInfo = fs.lstatSync(zipPath);
res = `version: ${program.ver || v || '1.0.0'}
files:
  - url: ${zipName}
    sha512: ${zipHash}
    size: ${zipInfo.size}
  - url: ${fileName.base}
    sha512: ${fileHash}
    size: ${fileInfo.size}
path: ${zipName}
sha512: ${zipHash}
releaseDate: '${dt}'
`;

end(out, res);

}).catch((err) => {
  console.log(err);
});

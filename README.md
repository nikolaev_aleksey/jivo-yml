# jivo-yml
YML file generator for updating.

## Install
```
npm install jivo-yml -g
```

## Usage
```
Options:
-f, --file <path>        file path for generate yml file
-c, --channel <channel>  update channel
-n --now                 set current datetime for releaseDate field
-v --ver <version>       program version
-F --force               update exists yml file
-h, --help               output usage information
```

## Example commands
### latest.yml
```
jivo-yml --file ./test/JivoSite-4.0.3.exe
```

### alpha.yml
```
jivo-yml --file ./test/JivoSite-4.0.3.exe -c alpha --ver 4.0.3
```

### beta-mac.yml  
```
jivo-yml --file ./test/JivoSite-4.0.3.dmg --channel beta
```

### beta.yml with force overwrire
```
jivo-yml --file ./test/JivoSite-4.0.3.exe -c beta --force
```

### latest-mac.yml with custom version
```
jivo-yml --file ./test/JivoSite-4.0.3.dmg --ver 4.0.4
```

### update releaseDate in latest.yml
```
jivo-yml --file ./test/JivoSite-4.0.3.exe --now --force
```

## Example *.yml file
```
version: 4.0.3
files:
  - url: JivoSite-4.0.3.exe
    sha512: 6Nuq/K+zNB4asdpp+bAwJ6fgWpnnE9MZoPNGeraJgkqHUIVf3NZ/q9Lj8BINiSD8LK7WgfZ4BzfD0IuiI7e3yw==
    size: 53968123
path: JivoSite-4.0.3.exe
sha512: 6Nuq/K+zNB4asdpp+bAwJ6fgWpnnE9MZoPNGeraJgkqHUIVf3NZ/q9Lj8BINiSD8LK7WgfZ4BzfD0IuiI7e3yw==
releaseDate: '2019-05-16T08:51:36.619Z'
```